# denys_test_project

## Installation
### Development Environment

In first place your environment should be set up for development, install the necessary packages for your environment. Some you might need to keep in mind:
* Python 3.4+
* PostgreSQL 9.4 
* Pip 
* Docker and Docker-Compose (If you want to run it on docker for development)

For setting up in Ubuntu this list should be a good starting point:
```
$ sudo apt-get install build-essential libssl-dev libffi-dev python-dev python python-virtualenv python-pip python3-venv libfreetype6 libfreetype6-dev pkg-config npm postgresql-client postgresql postgresql-contrib postgresql-server-dev-9.x git python3-dev
```
You can run this project either locally or with docker

## Setting Up
### Locally

#### Postgres

```
$ sudo su postgres -c psql

postgres$ create user <user> with password '<password>';

postgres$ create database <database> owner <user> encoding 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
```

#### Only for local purposes to use fabric

```
postgres$ alter user <user> with superuser;
```

Edit the `/etc/postgresql/9.x/main/pg_hba.conf` file as root user:

```
local all postgres trust

local all all trust
```

Restart the service

```
$ /etc/init.d/postgresql restart
```

### Configure the project
#### Create a virtualenv

```
$ python3 -m venv denys_test_project
```

This command will create a new folder with the name `denys_test_project`

Alternatively you can use `virtualenvwrapper` and use:
```
$ mkvirtualenv -p=python3 denys_test_project 
```

#### Clone the project

First verify your SSH Keys on bitbucket configuration `https://bitbucket.org/account/user/<your_user>/ssh-keys/`
then if you dont have a key that points to your computer follow this tutorials:

* https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html
* https://confluence.atlassian.com/bitbucket/add-an-ssh-key-to-an-account-302811853.html

```
$ git clone git@bitbucket.org:fincite/denys_test_project.git
```

#### Activate your enviroment
Inside the `denys_test_project` folder run the following command

```
$ source bin/activate
```

After this you will see the virtualenv name in your prompt. i.e.:

```
(denys_test_project) $
```

#### Install requirements
```
(denys_test_project)$ cd denys_test_project-backend

(denys_test_project)$ pip install -r requirements.txt
```

#### Setting up environment variables for project

For environment variables configuration, you will need a .env file in the parent directory of the current folder.

```
(denys_test_project) $ touch ../.env
```

An example configuration and how to obtain values for your virtualenv can be found in:
https://redmine.fincite.net/projects/management/wiki/Environment_variable_django_settings#Example

#### Run the project

Check that everything is well configured and run migrations:

```
(denys_test_project) $ ./manage.py check

(denys_test_project) $ ./manage.py migrate
```

Finally to run:

```
(denys_test_project) $ ./manage.py runserver
```

### With Docker

This project also can be run using Docker and Docker-Compose.

### Preconditions
Before the docker specific steps make sure that:
* The `.env` is correctly located one directory above the repository's files and is configured for the docker paths
  
  * The database url should be `psql://dbuser:dbpass@database:5432/denys_test_project` as this is the one that docker-compose will create

* Your RSA keys(the ones in .ssh/) are not configured with a password, otherwise build will fail. 
* This are set up in bitbucket so installation of private packages is possible

#### Build process

The first step will build the images using your own keys in order to be able to access the repositories when installing the requirements.

Enter the denys_test_project directory and run:

```
$ docker-compose build --build-arg SSH_KEY="$(cat ~/.ssh/id_rsa)" --build-arg SSH_KEY_PUB="$(cat ~/.ssh/id_rsa.pub)" project
```

#### Running the project

Once built, if everything went okay, you can start the project by executing:

```
$ docker-compose up -d
```
This will launch it in daemon mode (on the background)

To see the running container:
```
$ docker ps
```

To run the migrations or any other command you can use:
```
docker exec -it <container id or name from docker ps> <command>
```

i.e for migrations:

```
docker exec -it project python manage.py migrate
```

To shut down all the containers:
```
$ docker-compose down
```

> After configuring everything, the project should be running in localhost:8010, so be careful with port conflicts.

## Development Standards
### Run tests

Coverage is configured for the project for running tests and measuring in Scrutinizer

```
(denys_test_project) $ coverage run --source="." manage.py test --settings=denys_test_project.settings_test --verbosity=2
```

Or when running with docker:
```
docker exec -it project coverage run --source="." manage.py test --settings=denys_test_project.settings_test --verbosity=2
```

Once ran, if you want to see fast the results you can run

```
(denys_test_project) $ coverage report
```

or you can run

```
(denys_test_project) $ coverage html
```

and an HTML view of your test coverage will be generated in htmlcov/index.html

### Build documentation files

Sphinx is configured to build a user friendly site for code documentation.

To build this files run

```
(denys_test_project) $ cd docs
(denys_test_project) $ make html
```

They will be build in docs/build/html/ with index.html as the main page.
It can also be accessed from the admin site in the top navigation.
