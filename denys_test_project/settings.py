"""
Application Global Settings

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

import os
import environ

ROOT = environ.Path(__file__) - 2
ENV = environ.Env(
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, []),
    ENVIRONMENTS=(dict, {})
)
environ.Env.read_env('%s/.env' % str(ROOT - 1))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = ENV.bool('DEBUG')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = ROOT()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = ENV('SECRET_KEY')

ALLOWED_HOSTS = ENV.list('ALLOWED_HOSTS', default=[])

# Email Configuration

EMAIL_CONFIG = ENV.email_url('EMAIL_URL', default='consolemail://')
vars().update(EMAIL_CONFIG)

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': ENV.db()
}

DB_BACKUP_DIR = os.path.join(os.path.dirname(BASE_DIR), 'backups')

ENVIRONMENTS = ENV.json('ENVIRONMENTS')

# Application definition

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

THIRD_PARTY_APPS = [
    'rest_framework',
]

FINCITE_APPS = ['log_viewer']  # fabutils, log-viewer

PROJECT_APPS = ['charts']

ENV_APPS = ENV.list('APPS', default=[])

INSTALLED_APPS = (
    DJANGO_APPS + THIRD_PARTY_APPS + FINCITE_APPS + PROJECT_APPS + ENV_APPS
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'denys_test_project.urls'

FRONTEND_DIR = 'denys_test_project-frontend'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            # os.path.join(BASE_DIR, FRONTEND_DIR, 'build'),
            os.path.join(BASE_DIR, FRONTEND_DIR, 'build', 'static'),
            os.path.join(BASE_DIR, 'templates')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'denys_test_project.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, '../static')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, FRONTEND_DIR, 'build'),
    os.path.join(BASE_DIR, "docs", "build"),
    os.path.join(BASE_DIR, FRONTEND_DIR, 'build', 'static')
)

# Django REST

REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.AllowAny',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),
}

# Logging

LOGS_DIR = os.path.join(os.path.dirname(BASE_DIR), 'logs')
LOG_VIEWER_FILES = ['debug.log'] # Example

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'debug_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': os.path.join(
                BASE_DIR, os.path.join(LOGS_DIR, 'debug.log')),
            'formatter': 'verbose'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(levelname)s %(asctime)s](%(module)s, %(lineno)d) %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'loggers': {
        'debug': {
            'handlers': ['console', 'debug_file'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG')
        }
    }
}
