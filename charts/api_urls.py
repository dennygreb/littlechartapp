from django.urls import path

from .views import ChartView

urlpatterns = [
    path('data/', ChartView.as_view(), name="get_chart_data"),
]