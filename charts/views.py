from rest_framework.response import Response
from rest_framework.views import APIView
from core_sdk import CoreFactory
import logging


# Hardcoded allocation data
# {"isin": "XS1244060486", "quantity": 20},
# {"isin": "US01609W1027", "quantity": 10},
# {"isin": "US025816BD05", "quantity": 10},
# {"isin": "USP04808AN44", "quantity": 10},
# {"isin": "DE0005190003", "quantity": 10},
# {"isin": "AU000000CBA7", "quantity": 10},
# {"isin": "DE000DS3UTZ3", "quantity": 10},
# {"isin": "DE000DS060Q6", "quantity": 10},
# {"isin": "DE0005140008", "quantity": 10},
# {"isin": "NL0000288876", "quantity": 10},


class ChartView(APIView):
    
    def post(self, request):
        Core = CoreFactory.setup({})
        response = Core.Analyse.Allocation.by_components(request.data)
        return Response({'allocation': response})